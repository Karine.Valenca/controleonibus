package model;

public class Funcionario {
	private String nome;
	private String numeroIdentificacao;
	
	public Funcionario(){
		
	}
	
	public Funcionario(String nome, String numeroIdentificaco){
		this.nome = nome;
		this.numeroIdentificacao = numeroIdentificacao;
	}
	
	//getters e setters
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNumeroIdentificacao() {
		return numeroIdentificacao;
	}
	public void setNumeroIdentificacao(String numeroIdentificacao) {
		this.numeroIdentificacao = numeroIdentificacao;
	}
	
	
}
