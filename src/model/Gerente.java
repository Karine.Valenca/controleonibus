package model;

public class Gerente extends Funcionario{
	private String senha;
	
	public Gerente(){
		
	}
	
	public Gerente(String nome, String numeroIdentificacao,
			String senha){
		this.senha = senha;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
